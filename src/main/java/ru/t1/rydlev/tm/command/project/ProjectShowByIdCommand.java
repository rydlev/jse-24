package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

}
