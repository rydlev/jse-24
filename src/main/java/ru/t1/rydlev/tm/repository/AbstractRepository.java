package ru.t1.rydlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.repository.IRepository;
import ru.t1.rydlev.tm.model.AbstractModel;

import java.util.*;
import java.util.function.Predicate;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected Predicate<M> filterById(@Nullable final String id) {
        return m -> id.equals(m.getId());
    }

    @NotNull
    private final Map<String, M> models = new LinkedHashMap<>();

    @Override
    public void clear() {
        models.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(models.values());
        result.sort(comparator);
        return result;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        models.forEach(this::add);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        return models.get(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) {
        return models
                .values()
                .stream()
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<M> collection) {
        collection.stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

}
