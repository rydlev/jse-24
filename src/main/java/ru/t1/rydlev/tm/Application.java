package ru.t1.rydlev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.component.IBootstrap;
import ru.t1.rydlev.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable final String... args) {
        @NotNull final IBootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}